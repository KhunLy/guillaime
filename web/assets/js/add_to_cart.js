$(function() {
    let elem = document.querySelector('#add_to_cart_modal');
    let instance = M.Modal.init(elem);
    let lastId = null;
    let lastPrice = null;
    $('#results').on('click', '.add_to_cart', function(){
        instance.open();
        lastId = $(this).attr('target-id');
        lastPrice = parseFloat($(this).attr('target-price'));
        $("#total").html(lastPrice);
        $("#unitPrice").html(lastPrice);
        $('#quantity').val(1);
        $.ajax({
            url: Routing.generate('dish_card', {'id': lastId}),
            method: 'GET',
            success: function(data) {
                $('#chosen_dish').html(data.html);
            }
        })
    });

    $('#quantity').on('input', function(){
        let val = parseInt($(this).val());
        $("#factor").html(val);
        $("#total").html(lastPrice * val);
    });

    $('#add_to_cart_button').on('click', function(){
        $.ajax({
            url: Routing.generate('order_add_line', {'id': lastId}),
            data: {'quantity': parseInt($('#quantity').val())},
            method: 'POST',
            success: function(data) {
                toastr.success('Added to Cart');
                instance.close();
            },
            error: function(xhr) {
                toastr.error('Cannot update Cart');
            }
        });
    });
});