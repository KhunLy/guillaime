<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends Controller
{
    /**
     * @Route(name="login", path="/login")
     */
    public function loginAction(AuthenticationUtils $authUtils){
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->render(':user:login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }

    /**
     * @Route(name="register", path="/register")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder){
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $user->setRole('ROLE_CUSTOMER')
                ->setPassword($encoder->encodePassword($user, $user->getPlainPassword()));

            // remove this after email confirmation
            $user->setEmailVerified(true);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Correctly registered');
            return $this->redirectToRoute('default_home');
        }
        return $this->render(':user:register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route(name="admin_user_index", path="/admin/user/index")
     * @IsGranted("ROLE_ADMIN")
     */
    public function indexAction()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        return $this->render(':user:index.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route(name="admin_user_delete", path="/admin/user/{id}", requirements={"id": "\d+"}, options={"expose":true}, methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function adminDeleteAction(User $id)
    {
        $em = $this->getDoctrine()->getManager();
        if ($id != null && $id->getRole() != "ROLE_SA" && $id->getId() != $this->getUser()->getId()){
            $em->remove($id);
            $em->flush();
            return new JsonResponse(null, 200);
        }
        return new JsonResponse(null, 400);
    }

    /**
     * @Route(name="admin_user_active", path="/admin/user/active/{id}", requirements={"id": "\d+"}, options={"expose":true}, methods={"PATCH"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function adminPatchActiveAction(Request $request, User $id)
    {
        $em = $this->getDoctrine()->getManager();
        if ($id != null && $id->getRole() != "ROLE_SA" && $id->getId() != $this->getUser()->getId()){
            $id->setActive(!$id->isActive());
            $em->flush();
            return new JsonResponse($id->isActive(), 200);
        }
        return new JsonResponse($id->isActive(), 400);
    }

    /**
     * @Route(name="admin_user_email_checked", path="/admin/user/email_checked/{id}", requirements={"id": "\d+"}, options={"expose":true}, methods={"PATCH"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function adminPatchEmailCheckedAction(Request $request, User $id)
    {
        $em = $this->getDoctrine()->getManager();
        if ($id != null && $id->getRole() != "ROLE_SA" && $id->getId() != $this->getUser()->getId()){
            $id->setEmailVerified(!$id->isEmailVerified());
            $em->flush();
            return new JsonResponse($id->isEmailVerified(), 200);
        }
        return new JsonResponse($id->isEmailVerified(), 400);
    }

    /**
     * @Route(name="admin_user_role", path="/admin/user/role/{id}", requirements={"id": "\d+"}, options={"expose":true}, methods={"PATCH"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function adminPatchRoleAction(Request $request, User $id)
    {
        $authorizedRoles = ['ROLE_ADMIN', 'ROLE_EMPLOYEE', 'ROLE_CUSTOMER'];
        $role = $request->request->get('role');
        if (!array_search($role, $authorizedRoles)){
            return new JsonResponse($id->getRole(), 400);
        }
        $em = $this->getDoctrine()->getManager();
        if ($id != null && $id->getRole() != "ROLE_SA" && $id->getId() != $this->getUser()->getId()){
            $id->setRole($role);
            $em->flush();
            return new JsonResponse($id->getRole(), 200);
        }
        return new JsonResponse($id->getRole(), 400);
    }
}
