<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller
{
    /**
     * @Route(name="admin_category_index", path="/admin/category/index")
     */
    public function adminIndexAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            $this->addFlash('success', 'New Category Added');
        }
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAllOrdered();
        return $this->render(':category:index.html.twig', [
            'form' => $form->createView(),
            'categories' => $categories
        ]);
    }

    /**
     * @Route(name="admin_category_delete", path="/admin/category/{id}", requirements={"id": "\d+"}, options={"expose":true}, methods={"DELETE"})
     */
    public function adminDeleteAction(Category $id)
    {
        $em = $this->getDoctrine()->getManager();
        if ($id != null){
            $em->remove($id);
            $em->flush();
            return new JsonResponse(true, 200);
        }
        return new JsonResponse(false, 400);
    }

    /**
     * @Route(name="admin_category_sort", path="/admin/category/sort", options={"expose":true}, methods={"PATCH"})
     */
    public function adminSortAction(Request $request)
    {
        $ids = $request->request->get('ids');
        if ($ids == null){
            return new JsonResponse(false, 400);
        }
        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Category::class);
        foreach ($ids as $index => $id) {
            $cat = $repo->find($id);
            $cat->setOrder($index);
            $em->flush();
        }
        return new JsonResponse(true, 200);

    }
}
