<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 27/03/2019
 * Time: 02:15
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Comment;
use AppBundle\Entity\Dish;
use AppBundle\Filter\DishFilter;
use AppBundle\Form\CommentType;
use AppBundle\Form\DishFilterType;
use AppBundle\Form\DishType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DishController extends Controller
{
    /**
     * @Route(name="admin_dish_index", path="/admin/dish")
     */
    public function adminIndexAction()
    {
        $dishes = $this->getDoctrine()->getRepository(Dish::class)->findAll();
        return $this->render(':dish:index.html.twig', [
           'dishes' => $dishes
        ]);
    }

    /**
     * @Route(name="admin_dish_edit", path="/admin/dish/edit/{id}", defaults={"id": 0}, requirements={"id": "\d+"})
     */
    public function adminEditAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Dish');
        $dish = $repo->find($id) ?: new Dish();
        $form = $this->createForm(DishType::class, $dish);
        $form->handleRequest($request);
        //dump($dish);die;
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($dish);
            $em->flush();
            $this->addFlash('success','Dishes update');
            return $this->redirectToRoute('admin_dish_index');
        }
        return $this->render(':dish:edit.html.twig', [
            'form' => $form->createView(),
            'dish' => $dish
        ]);
    }

    /**
     * @Route(name="admin_dish_delete", path="/admin/dish/{id}", requirements={"id": "\d+"}, options={"expose":true}, methods={"DELETE"})
     */
    public function adminDeleteAction(Dish $id)
    {
        $em = $this->getDoctrine()->getManager();
        if ($id != null){
            $em->remove($id);
            $em->flush();
            return new JsonResponse(true, 200);
        }
        return new JsonResponse(true, 400);
    }

    /**
     * @Route(name="dish_filter", path="/dish/filter")
     */
    public function searchAction(Request $request)
    {
        $dish = new DishFilter();
        $form = $this->createForm(DishFilterType::class, $dish);
        $form->handleRequest($request);
        $dishes = $this->getDoctrine()->getRepository('AppBundle:Dish')
            ->findWithFilter($dish);
        return $this->render(':dish:filter.html.twig', [
            'form' => $form->createView(),
            'dishes' => $dishes
        ]);
    }

    /**
     * @Route(name="dish_filter_ajax", path="/dish/filter/ajax", options={"expose": true}, methods={"POST"})
     */
    public function searchAjaxAction(Request $request)
    {
        $dish = new DishFilter();
        $form = $this->createForm(DishFilterType::class, $dish);
        $form->handleRequest($request);
        $dishes = $this->getDoctrine()->getRepository('AppBundle:Dish')
            ->findWithFilter($dish);
        $view = $this->renderView(':dish:list.html.twig', [
            'dishes' => $dishes
        ]);
        return new JsonResponse(['results' => $view], 200);
    }

    /**
     * @Route(name="dish_last", path="/dish/last")
     */
    public function lastAction()
    {
        $dishes = $this->getDoctrine()->getRepository('AppBundle:Dish')
            ->findLast();
        return $this->render(':dish:last.html.twig', [
            'dishes' => $dishes
        ]);
    }

    /**
     * @Route(name="detail_dish", path="/dish/detail/{id}", requirements={"id": "\d+"})
     */
    public function detailAction(Request $request, $id)
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        $dish = $this->getDoctrine()->getRepository('AppBundle:Dish')
            ->find($id);
        if ($form->isSubmitted() && $form->isValid()){
            $comment->setDish($dish);
            $comment->setUser($this->getUser());
            $comment->setDate(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            $this->addFlash('success', 'Comments updated');
            $form = $this->createForm(CommentType::class, (new Comment())->setRating(2.5));
        }


        return $this->render(':dish:detail.html.twig', [
            'dish' => $dish,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route(name="dish_card", path="/dish/card/{id}", requirements={"id": "\d+"}, options={"expose":true}, methods={"GET"})
     */
    public function dishCardAction(Dish $id)
    {
        if ($id != null){
            $view = $this->renderView(':dish:dish_card.html.twig', [
                'dish' => $id
            ]);
            return new JsonResponse(['html' => $view], 200);
        }
        return new JsonResponse(null, 400);

    }
}