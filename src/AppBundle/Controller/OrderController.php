<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Dish;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderLine;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class OrderController extends Controller
{
    /**
     * @Route(
     *     name="order_index",
     *     path="/order/index"
     * )
     * @IsGranted("ROLE_CUSTOMER")
     */
    public function indexAction()
    {
        $order = $this->getDoctrine()->getRepository('AppBundle:Order')->findInProgess($this->getUser());
        return $this->render(':order:index.html.twig', [
            'order' => $order
        ]);
    }

    /**
     * @Route(name="order_add_line", path="/order/add/line/{id}", methods={"POST"}, options={"expose": true})
     * @IsGranted("ROLE_CUSTOMER")
     */
    public function addToCart(Request $request, Dish $id)
    {
        $order = $this->getDoctrine()->getRepository('AppBundle:Order')->findInProgess($this->getUser());
        if ($order == null){
            $order = new Order();
            $date = new \DateTime();
            $repo = $this->getDoctrine()->getRepository(Order::class);
            $count = $repo->getCountOfOrderByMonth();
            $ref = sprintf(
                '%s-%\'.04d',
                $date->format('ym'),
                $count + 1
            );
            $order->setUser($this->getUser())
                ->setState('IN_PROGRESS')
                ->setRef($ref)
                ->setDate($date);
            ;
        }
        $em = $this->getDoctrine()->getManager();
        $orderLine = new OrderLine();
        $orderLine->setOrder($order);
        $orderLine->setDish($id);
        $orderLine->setQuantity($request->request->get('quantity'));
        $orderLine->setUnitPrice($id->getDiscountPrice());
        $em->persist($order);
        $em->persist($orderLine);
        try{
            $em->flush();
            return new JsonResponse(null, 200);
        }
        catch (Exception $e){
            return new JsonResponse(null, 400);
        }
    }
}
