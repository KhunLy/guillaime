<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Form\MessageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route(name="default_home", path="/")
     */
    public function homeAction()
    {
        return $this->render(':default:home.html.twig');
    }

    /**
     * @Route(name="default_disclaimer", path="/disclaimer")
     */
    public function disclaimerAction()
    {
        return $this->render(':default:disclaimer.html.twig');
    }

    /**
     * @Route(name="default_contact", path="/contact")
     */
    public function contactAction(Request $request, \Swift_Mailer $mailer)
    {
        $message = new Message();
        if ($this->getUser() != null){
            $message->setEmail($this->getUser()->getUsername());
        }
        $form = $this->createForm(MessageType::class, $message, ["method" => "post"]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $message = (new \Swift_Message('Hello Email'))
                ->setFrom($this->getParameter('mailer_user'))
                ->setTo($this->getParameter('mailer_user'))
                ->setBody(
                    $this->renderView(
                    // templates/emails/registration.html.twig
                        ':emails:contact.html.twig',
                        ['message' => $message]
                    ),
                    'text/html'
                )

            ;

            try{
                $mailer->send($message);
                $this->addFlash('success', 'Email sent');
            }
            catch(\Exception $ex) {
                $this->addFlash('error', $ex->getMessage());
            }
        }
        return $this->render(':default:contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
