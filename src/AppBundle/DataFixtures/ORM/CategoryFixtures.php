<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 31/03/2019
 * Time: 12:22
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $categories = [
            'Maki',
            'Sushi',
            'Temaki',
            'Chirachi',
            'Tempura',
            'Teriyaki',
            'Soft Drink',
            'Alcoholic Drink'
        ];
        foreach ($categories as $i => $item){
            $c = new Category();
            $c->setName($item);
            $c->setOrder($i);
            $manager->persist($c);
        }
        $manager->flush();
    }
}