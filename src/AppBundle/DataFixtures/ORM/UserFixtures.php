<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 31/03/2019
 * Time: 11:58
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserFixtures extends Fixture implements ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the container.
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $data = array(
            array('guillaume.oger.bstorm@gmail.be', 'test1234=', 'ROLE_SA'),
            array('lykhun@gmail.com', 'test1234=', 'ROLE_ADMIN'),
            array('employee@domain.be', 'test1234=', 'ROLE_EMPLOYEE'),
            array('customer@domain.be', 'test1234=', 'ROLE_CUSTOMER'),
        );

        foreach ($data as $item) {
            $user = new User();

            $user->setEmail($item[0]);
            $user->setRole($item[2]);
            $encoder = $this->container->get('security.password_encoder');
            $encodedPwd = $encoder->encodePassword($user, $item[1]);
            $user->setPassword($encodedPwd);
            $user->setActive(true);
            $user->setEmailVerified(true);

            $user->setAddressLine('');
            $user->setZipCode('');
            $user->setCity('');




            $manager->persist($user);
        }
        $manager->flush();
    }
}