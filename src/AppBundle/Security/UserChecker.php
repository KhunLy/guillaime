<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 30/03/2019
 * Time: 16:46
 */

namespace AppBundle\Security;


use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\AuthenticationExpiredException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }

        // user is deleted, show a generic Account Not Found message.
        if (!$user->isActive()) {
            throw new AuthenticationException();
        }

        if (!$user->isEmailVerified()){
            throw new AuthenticationException('User Email not verified');
        }
    }

    public function checkPostAuth(UserInterface $user)
    {

    }
}