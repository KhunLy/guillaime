<?php

namespace AppBundle\Form;

use AppBundle\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DishFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name',
                'required' => false
            ])
            ->add('categories', EntityType::class, [
                'label' => 'Categories',
                'choice_label' => 'name',
                'class' => 'AppBundle\Entity\Category',
                'multiple' => true,
                'required' => false,
                'query_builder' => function(CategoryRepository $repo) {
                    return $repo->getFindAllOrderedQb();
                }
            ])
            ->add('onDiscount', CheckboxType::class, [
                'label' => 'On Discount',
                'required' => false
            ])
            ->add('highlighted', CheckboxType::class, [
                'label' => 'Highlighted',
                'required' => false
            ])
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Filter\DishFilter'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_dish';
    }


}
