<?php

namespace AppBundle\Form;

use AppBundle\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DishType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('picture', PictureType::class, [
                'label' => null
            ])
            ->add('name', TextType::class, [
                'label' => 'Name',
                'required' => true
            ])
            ->add('priceInclTax', NumberType::class, [
                'label' => 'Price',
                'required' => true
            ])
            ->add('vat', ChoiceType::class, [
                'label' => 'VAT',
                'choices' => [
                    '0%' => 0,
                    '6%' => 6,
                    '18%' => 18,
                    '21%' => 21
                ],
                'required' => true
            ])
            ->add('discount', NumberType::class, [
                'label' => 'Discount',
                'required' => false
            ])
            ->add('category', EntityType::class, [
                'label' => 'Category',
                'choice_label' => 'name',
                'class' => 'AppBundle\Entity\Category',
                'query_builder' => function(CategoryRepository $repo) {
                    return $repo->getFindAllOrderedQb();
                }
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'Active',
                'required' => false
            ])
            ->add('highlighted', CheckboxType::class, [
                'label' => 'Is Highlighted',
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Create'
            ])
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Dish'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_dish';
    }


}
