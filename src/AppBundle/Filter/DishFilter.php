<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 31/03/2019
 * Time: 15:16
 */

namespace AppBundle\Filter;


use AppBundle\Entity\Category;

class DishFilter
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Category[]
     */
    private $categories;

    /**
     * @var boolean
     */
    private $onDiscount;

    /**
     * @var boolean
     */
    private $highlighted = true;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return DishFilter
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Category[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Category[] $categories
     * @return DishFilter
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOnDiscount()
    {
        return $this->onDiscount;
    }

    /**
     * @param bool $onDiscount
     * @return DishFilter
     */
    public function setOnDiscount($onDiscount)
    {
        $this->onDiscount = $onDiscount;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHighlighted()
    {
        return $this->highlighted;
    }

    /**
     * @param bool $highlighted
     * @return DishFilter
     */
    public function setHighlighted($highlighted)
    {
        $this->highlighted = $highlighted;
        return $this;
    }
}