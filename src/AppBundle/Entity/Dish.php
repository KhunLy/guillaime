<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Dish
 *
 * @ORM\Table(name="dish")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DishRepository")
 * @UniqueEntity("name")
 */
class Dish
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=false, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $name;

    /**
     * @var float
     * @ORM\Column(name="price_incl_tax", type="decimal", nullable=false)
     * @Assert\NotBlank()
     */
    private $priceInclTax;

    /**
     * @var boolean
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = true;

    /**
     * @var boolean
     * @ORM\Column(name="highlighted", type="boolean", nullable=false)
     */
    private $highlighted = true;

    /**
     * @var integer
     * @ORM\Column(name="vat", type="integer", nullable=false)
     */
    private $vat;

    /**
     * @var integer
     * @ORM\Column(name="discount", type="integer", nullable=true)
     */
    private $discount = 0;

    /**
     * @var Picture
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Picture", cascade={"remove", "persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $picture;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $category;

    /**
     * @var Comment[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Comment", mappedBy="dish")
     */
    private $comments;

    public function __construct()
    {
        $this->comments = [];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Dish
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceInclTax()
    {
        return $this->priceInclTax;
    }

    /**
     * @param float $priceInclTax
     * @return Dish
     */
    public function setPriceInclTax($priceInclTax)
    {
        $this->priceInclTax = $priceInclTax;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return Dish
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHighlighted()
    {
        return $this->highlighted;
    }

    /**
     * @param bool $highlighted
     * @return Dish
     */
    public function setHighlighted($highlighted)
    {
        $this->highlighted = $highlighted;
        return $this;
    }



    /**
     * @return int
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param int $vat
     * @return Dish
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

    /**
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param int $discount
     * @return Dish
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }



    /**
     * @return Picture
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param Picture $picture
     * @return Dish
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Dish
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return Comment[]
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param Comment $comment
     * @return Dish
     */
    public function addComment($comment)
    {
        $this->comments[] = $comment;
        $comment->setDish($this);
        return $this;
    }

    /**
     * @return float|int
     */
    public function getAverageRating()
    {
        return array_sum(array_map(function($item) {
            /**@var $item Comment*/
            return $item->getRating();
        }, $this->getComments()->toArray())) / count($this->getComments()->toArray());
    }

    public function getDiscountPrice()
    {
        return $this->getPriceInclTax() - ($this->getPriceInclTax() * $this->getDiscount() / 100);
    }
}

