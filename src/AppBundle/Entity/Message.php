<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 27/03/2019
 * Time: 22:28
 */

namespace AppBundle\Entity;


use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Message
 */
class Message
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $object;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Message
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param string $object
     * @return Message
     */
    public function setObject($object)
    {
        $this->object = $object;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }


}