<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OrderLine
 *
 * @ORM\Table(name="order_line")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderLineRepository")
 */
class OrderLine
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Dish
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Dish")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $dish;

    /**
     * @var Order
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order", inversedBy="orderLines")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $order;

    /**
     * @var float
     * @ORM\Column(name="unit_price", type="decimal", nullable=false)
     */
    private $unitPrice;

    /**
     * @var integer
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     * @Assert\Range(min="1")
     */
    private $quantity;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * @param Dish $dish
     * @return OrderLine
     */
    public function setDish($dish)
    {
        $this->dish = $dish;
        return $this;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     * @return OrderLine
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return OrderLine
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return OrderLine
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /* additional methods */
    public function getSubTotal()
    {
        return $this->unitPrice * $this->quantity;
    }

    public function getExlTaxSubTotal()
    {
        $vat = $this->getDish()->getVat();
        return ($this->getSubTotal() * (100 - $vat)) / 100;
    }
    /* end additional methods */
}

