<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Order
 *
 * @ORM\Table(name="`order`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderRepository")
 * @UniqueEntity("ref")
 */
class Order
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\Length(25)
     * @ORM\Column(name="ref", type="string", length=255, nullable=false, unique=true)
     */
    private $ref;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="SET NULL", nullable=true)
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(name="state", type="string", length=50, nullable=false)
     */
    private $state;

    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var OrderLine[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OrderLine", mappedBy="order")
     */
    private $orderLines;

    public function __construct()
    {
        $this->orderLines = [];
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param string $ref
     * @return Order
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
        return $this;
    }



    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Order
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @return Order
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return Order
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return OrderLine[]
     */
    public function getOrderLines()
    {
        return $this->orderLines;
    }

    /**
     * @param OrderLine $orderLine
     * @return Order
     */
    public function addOrderLine($orderLine)
    {
        $this->orderLines[] = $orderLine;
        return $this;
    }

    /* additional methods */
    public function getTotal()
    {
        return array_sum(array_map(function($item){
            /**@var $item OrderLine*/
            return $item->getSubTotal();
        }, $this->orderLines));
    }
    public function getExclTaxTotal()
    {
        return array_sum(array_map(function($item){
            /**@var $item OrderLine*/
            return $item->getExlTaxSubTotal();
        }, $this->orderLines));
    }
    /* end additional methods */
}

